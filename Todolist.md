# TO Do List

*This is the to-do list for all the task pending in the project. Ones you take please mark them reserved, open tasks should be kept unchecked and completed tasks marked close.

This is not an intensive list, if you find something missing, incomplete or broken feel free to update the file. Also it would be nice to have a small problem introduction, files to look at and where to start to make it easy for others to start.*

- [x] Some task 1 (completed)
- [ ] Some task 2 (incomplete)


---------------------------------------
Tasks
---------------------------------------
- [x] Chat                  (incomplete), Providing group chat with real time update using sockets, chats will be based on projects.
- [x] Projects              (incomplete), All CRUD for project Object and it's subobjects e.g. stories, tasks etc.
- [x] Google OAuth          (complete), Adding google authentication to fetch the user and profile.
- [x] Adding Email Facility (incomplete), Interface and server side is already provided, just create the services and connect to interface.
- [ ] Sprint Board          (haven't started), Please make some interface and attach it to server that imitates the real sprint board.
- [ ] Dashboard             (haven't started), making the dashboard including overview of tasks done and pending, projects involvement and other alerts.
- [ ] Tooltips              (incomplete), Tooltips on top right with unread email notifications and pending tasks.
- [ ] Facebook OAuth        (haven't started), not mandatory but if someone have time can add.
- [ ] Profile               (haven't started), not mandatory but if someone have time can add, or it's for future.
- [ ] Search Facility       (haven't started), not mandatory but if someone have time can add, or it's for future.
