import passport from 'passport';
import {OAuth2Strategy as GoogleStrategy} from 'passport-google-oauth';

export function setup(User, config) {
  passport.use(new GoogleStrategy({
    clientID: /*'204591661296-h2vtmbdo1mpnomdi44eahsc2dlvhqafv.apps.googleusercontent.com',*/config.google.clientID,
    clientSecret: /*'Ay8CBXmw5zR00FHHNAYwBy0d',*/config.google.clientSecret,
    callbackURL: /*'http://localhost:9000/auth/google/callback'*/config.google.callbackURL
  },
  function(accessToken, refreshToken, profile, done) {
    User.findOneAsync({
      'google.id': profile.id
    })
      .then(user => {
        if (user) {
          return done(null, user);
        }

        user = new User({
          name: profile.displayName,
          email: profile.emails[0].value,
          role: 'user',
          username: profile.emails[0].value.split('@')[0],
          provider: 'google',
          google: profile._json
        });
        user.saveAsync()
          .then(user => done(null, user))
          .catch(err => done(err));
      })
      .catch(err => done(err));
  }));
}
