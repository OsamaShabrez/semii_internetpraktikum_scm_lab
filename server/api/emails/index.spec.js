'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var emailsCtrlStub = {
  index: 'emailsCtrl.index',
  show: 'emailsCtrl.show',
  create: 'emailsCtrl.create',
  update: 'emailsCtrl.update',
  destroy: 'emailsCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var emailsIndex = proxyquire('./index.js', {
  'express': {
    Router: function() {
      return routerStub;
    }
  },
  './emails.controller': emailsCtrlStub
});

describe('Emails API Router:', function() {

  it('should return an express router instance', function() {
    emailsIndex.should.equal(routerStub);
  });

  describe('GET /api/emails', function() {

    it('should route to emails.controller.index', function() {
      routerStub.get
        .withArgs('/', 'emailsCtrl.index')
        .should.have.been.calledOnce;
    });

  });

  describe('GET /api/emails/:id', function() {

    it('should route to emails.controller.show', function() {
      routerStub.get
        .withArgs('/:id', 'emailsCtrl.show')
        .should.have.been.calledOnce;
    });

  });

  describe('POST /api/emails', function() {

    it('should route to emails.controller.create', function() {
      routerStub.post
        .withArgs('/', 'emailsCtrl.create')
        .should.have.been.calledOnce;
    });

  });

  describe('PUT /api/emails/:id', function() {

    it('should route to emails.controller.update', function() {
      routerStub.put
        .withArgs('/:id', 'emailsCtrl.update')
        .should.have.been.calledOnce;
    });

  });

  describe('PATCH /api/emails/:id', function() {

    it('should route to emails.controller.update', function() {
      routerStub.patch
        .withArgs('/:id', 'emailsCtrl.update')
        .should.have.been.calledOnce;
    });

  });

  describe('DELETE /api/emails/:id', function() {

    it('should route to emails.controller.destroy', function() {
      routerStub.delete
        .withArgs('/:id', 'emailsCtrl.destroy')
        .should.have.been.calledOnce;
    });

  });

});
