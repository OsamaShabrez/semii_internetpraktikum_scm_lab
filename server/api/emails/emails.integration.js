'use strict';

var app = require('../..');
import request from 'supertest';

var newEmails;

describe('Emails API:', function() {

  describe('GET /api/emails', function() {
    var emailss;

    beforeEach(function(done) {
      request(app)
        .get('/api/emails')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          emailss = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      emailss.should.be.instanceOf(Array);
    });

  });

  describe('POST /api/emails', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/emails')
        .send({
          name: 'New Emails',
          info: 'This is the brand new emails!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          newEmails = res.body;
          done();
        });
    });

    it('should respond with the newly created emails', function() {
      newEmails.name.should.equal('New Emails');
      newEmails.info.should.equal('This is the brand new emails!!!');
    });

  });

  describe('GET /api/emails/:id', function() {
    var emails;

    beforeEach(function(done) {
      request(app)
        .get('/api/emails/' + newEmails._id)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          emails = res.body;
          done();
        });
    });

    afterEach(function() {
      emails = {};
    });

    it('should respond with the requested emails', function() {
      emails.name.should.equal('New Emails');
      emails.info.should.equal('This is the brand new emails!!!');
    });

  });

  describe('PUT /api/emails/:id', function() {
    var updatedEmails;

    beforeEach(function(done) {
      request(app)
        .put('/api/emails/' + newEmails._id)
        .send({
          name: 'Updated Emails',
          info: 'This is the updated emails!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }
          updatedEmails = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedEmails = {};
    });

    it('should respond with the updated emails', function() {
      updatedEmails.name.should.equal('Updated Emails');
      updatedEmails.info.should.equal('This is the updated emails!!!');
    });

  });

  describe('DELETE /api/emails/:id', function() {

    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete('/api/emails/' + newEmails._id)
        .expect(204)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when emails does not exist', function(done) {
      request(app)
        .delete('/api/emails/' + newEmails._id)
        .expect(404)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

  });

});
