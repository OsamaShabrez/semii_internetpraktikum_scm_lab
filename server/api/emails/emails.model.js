'use strict';

var mongoose = require('bluebird').promisifyAll(require('mongoose'));

var EmailsSchema = new mongoose.Schema({
    sender : String,
    receiver : String,
    cc : String,
    subject : String,
    message : String,
    attachment : String,
    time : {type : Date, default : Date.now},
    status : Boolean,
    trash : Boolean,
    junk : Boolean,
    draft : Boolean
});

export default mongoose.model('Emails', EmailsSchema);
