'use strict';

var mongoose = require('bluebird').promisifyAll(require('mongoose'));
//var shortid = require('shortid');

var ProjectSchema = new mongoose.Schema({
  name: String,
  description: String,
  owner: String,//{ type: mongoose.Schema.Types.ObjectId, ref: 'User'},
  master: String,//{ type: mongoose.Schema.Types.ObjectId, ref: 'User'},
  dateadded: { type: Date, default: Date.now},
  done: { type: Boolean, default: 0},
  inprogress: { type: Boolean, default: 0},
  developers :
  [

        //{ type: mongoose.Schema.Types.ObjectId, ref: 'User' },
        { type: String }

  ],
  stories :
  [
    {
      name : String,
      description : String,
      priority : Number,
      dateadded: { type: Date, default: Date.now},
      notstarted: { type: Boolean, default: 1},
      inprogress: { type: Boolean, default: 0},
      done: { type: Boolean, default: 0},
      tasks :
      [
        {
          name : String,
          assigne : String,
          details : String,
          dateadded: { type: Date, default: Date.now},
          notstarted: { type: Boolean, default: 1},
          inprogress: { type: Boolean, default: 0},
          done: { type: Boolean, default: 0}
        }
      ]
    }
  ]

});

export default mongoose.model('Project', ProjectSchema);
