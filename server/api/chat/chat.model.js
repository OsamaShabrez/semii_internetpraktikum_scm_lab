'use strict';

var mongoose = require('bluebird').promisifyAll(require('mongoose'));

var ChatSchema = new mongoose.Schema({
  projectid: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Project'
  },
  messages: [{
    userName: String,
    message: String,
    date: {
      type: Date,
      default: Date.now
    },
    time: String
  }]
});

export default mongoose.model('Chat', ChatSchema);
