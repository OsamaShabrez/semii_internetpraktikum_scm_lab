/**
 * Populate DB with sample data on server start
 * to disable, edit config/environment/index.js, and set `seedDB: false`
 */

'use strict';
import Thing from '../api/thing/thing.model';
import User from '../api/user/user.model';
import Project from '../api/project/project.model';
import Chat from '../api/chat/chat.model'

Thing.find({}).removeAsync()
  .then(() => {
    Thing.create({
      name: 'Development Tool',
      info: 'Integration with popular tools such as Bower, Grunt, Babel, Karma, ' +
        'Mocha, JSHint, Node Inspector, Livereload, Protractor, Jade, ' +
        'Stylus, Sass, and Less.'
    }, {
      name: 'Server and Client integration',
      info: 'Built with a powerful and fun stack: MongoDB, Express, ' +
        'AngularJS, and Node.'
    }, {
      name: 'Smart Build System',
      info: 'Build system ignores `spec` files, allowing you to keep ' +
        'tests alongside code. Automatic injection of scripts and ' +
        'styles into your index.html'
    }, {
      name: 'Modular Structure',
      info: 'Best practice client and server structures allow for more ' +
        'code reusability and maximum scalability'
    }, {
      name: 'Optimized Build',
      info: 'Build process packs up your templates as a single JavaScript ' +
        'payload, minifies your scripts/css/images, and rewrites asset ' +
        'names for caching.'
    }, {
      name: 'Deployment Ready',
      info: 'Easily deploy your app to Heroku or Openshift with the heroku ' +
        'and openshift subgenerators'
    });
  })
  .then(() => {
    console.log('finished populating Things');
  });

User.find({}).removeAsync()
  .then(() => {
    User.create({
      _id: "56cb598bb70682501bafa9df",
      salt: "ilJJxG94G1B+tfNAn2X3nw==",
      provider: "local",
      name: "Test User",
      email: "test@example.com",
      password: "test",
      role: "user",
      __v: 0
    }, {
      _id: "56cb598bb70682501bafa9e0",
      salt: "1CJ9vAzRIQ0Rm5h8xgSilw==",
      provider: "local",
      name: "Admin",
      email: "admin@example.com",
      password: "admin",
      pic: "assets/images/user3-128x128.jpg",
      role: "admin",
      __v: 0
    }, {
      _id: "56cb5a82b70682501bafa9e1",
      salt: "BtxAhKAipK4lmA94Dhg/MQ==",
      provider: "local",
      name: "M. M. Bilal Shabbir",
      email: "mmbspk@gmail.com",
      password: "mmbspk",
      pic: "assets/images/user2-160x160.jpg",
      role: "user",
      __v: 0
    }, {
      _id: "56d7967745ab81322a964927",
      salt: "cNSq3gR4zPcXmrmEru0mtA==",
      provider: "local",
      name: "Osama Shabrez",
      email: "osamashabrez@live.com",
      password: "admin",
      pic: "assets/images/user3-128x128.jpg",
      role: "user",
      __v: 0
    }, {
      _id: "56d7967745ab53825a752874",
      salt: "cNSq3gR4zPcXmrmEru0mtA==",
      provider: "local",
      name: "Arsalan Siddiqi",
      email: "arsalan.siddiqi@live.com",
      password: "arsalan",
      pic: "assets/images/user3-128x128.jpg",
      role: "user",
      __v: 0
    })
  })
  .then(() => {
    console.log('finished populating Users');
  });

Project.find({}).removeAsync()
  .then(() => {
    Project.create({
      _id: "56cb2f1362309ca17358410d",
      name: "My Awesome Project",
      description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
      owner: "M. M. Bilal Shabbir",
      master: "Osama Shabrez",
      done: 0,
      developers: ["Admin", "Test User"],
      stories: [{
        name: "My Awesome Story No. 1",
        description: "Awesome because these is no story",
        priority: 3,
        tasks: [{
          name: "Do nothing",
          details: "The description and details about the task are nothing because it is a do nothing story",
          assigne: "Test User"
        }, {
          name: "Do everything, actually just look busy",
          details: "Try to look busy...",
          assigne: "Admin"
        }]
      }, {
        name: "Just another simple story",
        description: "A simple story is the best story",
        priority: 2,
        tasks: [{
          name: "My Story No 1",
          details: "The description and details about the task are yet to be filled",
          assigne: "Admin"
        }, {
          name: "My Story No 3 as we have missed 2",
          details: "There is no description....",
          assigne: "Test User"
        }, {
          name: "One awesome task right here",
          details: "just become awesome!",
          assigne: "Test User"
        }]
      }]
    })
  })
  .then(() => {
    console.log('finished populating Projects');
  });

Chat.find({}).removeAsync()
  .then(() => {
    Chat.create({
      _id: "56dba69b598ece575cd2074b",
      projectid: "56cb2f1362309ca17358410d",
      messages: [{
        userName: "Osama Shabrez",
        message: "Hello Team!",
        _id: "56dba711598ece575cd2074d",
        date: "2016-03-06T03:42:09.286Z"
      }, {
        userName: "Osama Shabrez",
        message: "Welcome aboard!",
        _id: "56dba711598ece575cd2074d",
        date: "2016-03-06T03:42:19.286Z"
      }, {
        userName: "Osama Shabrez",
        message: "We are awesome.",
        _id: "56dba711598ece575cd2074d",
        date: "2016-03-06T03:42:29.286Z"
      }, {
        userName: "Osama Shabrez",
        message: "Let's get to work.",
        _id: "56dba711598ece575cd2074d",
        date: "2016-03-06T03:42:39.286Z"
      }, {
        userName: "Arsalan Siddiqi",
        message: "I'm Batman",
        _id: "56dba711598ece575cd2074d",
        date: "2016-03-06T03:42:49.286Z"
      }, {
        userName: "M. M. Bilal Shabbir",
        message: "I have a question?",
        _id: "56dba711598ece575cd2074d",
        date: "2016-03-06T03:42:59.286Z"
      }, {
        userName: "M. M. Bilal Shabbir",
        message: "Just one..",
        _id: "56dba711598ece575cd2074d",
        date: "2016-03-06T03:43:09.286Z"
      }, {
        userName: "Osama Shabrez",
        message: "No questions please. Just do your work.",
        _id: "56dba711598ece575cd2074d",
        date: "2016-03-06T03:43:19.286Z"
      }, {
        userName: "M. M. Bilal Shabbir",
        message: "OK :(",
        _id: "56dba711598ece575cd2074d",
        date: "2016-03-06T03:44:09.286Z"
      }, {
        userName: "Arsalan Siddiqi",
        message: "haha :p",
        _id: "56dba711598ece575cd2074d",
        date: "2016-03-06T03:45:19.286Z"
      }]
    })
  })
  .then(() => {
    console.log('finished populating Chats');
  });
