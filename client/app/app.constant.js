(function(angular, undefined) {
'use strict';

angular.module('a2MeApp.constants', [])

.constant('appConfig', {userRoles:['guest','user','admin']})

;
})(angular);