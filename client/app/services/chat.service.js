'use strict';

angular.module('a2MeApp')
  .factory('ChatService', function($resource) {
      return $resource(
        'api/chats/projectid/:id', {
          id: '@id'
        }, {
          "update": {
            method: 'POST',
            params: {id: '@id'}
          },
          "create": {
            url: 'api/chats/',
            method: 'POST'
          }
        }
      );
  });
