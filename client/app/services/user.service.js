'use strict';

angular.module('a2MeApp')
.factory('UserService', function($resource){
  return $resource('api/users/:id', {
    id: '@id'
  });

});
