'use strict';

angular.module('a2MeApp')
.factory('ProjectService', function($resource){
  return $resource('api/projects/:id', {
    id: '@id'
  },{
    update: {
      method: 'PUT'
    }

  });

});
