'use strict';

angular.module('a2MeApp')
.factory('EmailService', function($resource){
  return $resource('api/emails/:id', {
    id: '@id'
  },{
    update: {
      method: 'PUT'
    }

  });

});
