'use strict';

angular.module('a2MeApp')
  .controller('TaskCtrl', function ($scope, $stateParams, $mdDialog, $mdToast, $state, socket, ProjectService ) {
    //$scope.message = 'Hello';

    $scope.goBack = function(){
      window.history.back();
    }

    ProjectService.get({id:$stateParams.id}, function(project){
      $scope.project = project;
    });

  /*  $scope.goToStory = function(story){
      $state.go('task', {
        id : story._id
      });
    }*/

    $scope.deleteStory = function(story, event){
      var confirm = $mdDialog.confirm()
      .title('Delete Story')
      .textContent('Are you sure you want to delete the Story')
      .ariaLabel('Delete')
      .targetEvent(event)
      .openFrom('#left')
      .ok('Yes, Please')
      .cancel('Not Today');

      $mdDialog.show(confirm).then(function(){
        //ProjectService.delete({id: project._id}, function(project){
          //console.log("Project Deleted");
        //});
        _.remove($scope.project.stories, function(story){
          return story._id === $scope.editStory._id;
        });

      ProjectService.update({
        id: $scope.project._id
      }, $scope.project, function(project) {
        $scope.project = project;
        var toast = $mdToast.simple()
        .textContent('Story Deleted')
        .action('OK')
        .highlightAction(false)
        .position('top');
        $mdToast.show(toast);
        $scope.editingStory = undefined;
      });

    });

      socket.syncUpdates('project', $scope.myProjects);
    }

    $scope.editStory = function(story){
      $scope.editswitch = story;
    }

    $scope.editTask = function(task){
      $scope.editswitch = task;
    }

    $scope.undoStoryEdit = function() {
      $scope.editswitch = undefined;
    }


  });
