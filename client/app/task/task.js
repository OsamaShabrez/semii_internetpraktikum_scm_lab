'use strict';

angular.module('a2MeApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('task', {
        url: '/task/:id',
        templateUrl: 'app/task/task.html',
        controller: 'TaskCtrl'
      });
  });
