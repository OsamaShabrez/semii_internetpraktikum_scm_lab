'use strict';

angular.module('a2MeApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('sprint', {
        url: '/sprint',
        templateUrl: 'app/sprint/sprint.html',
        controller: 'SprintCtrl'
      });
  });
