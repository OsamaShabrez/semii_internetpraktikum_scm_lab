'use strict';

angular.module('a2MeApp')
  .controller('SprintCtrl', function ($scope, socket, ProjectService) {

    ProjectService.query(function(projects){
      $scope.myProjects = projects;
      socket.syncUpdates('project', $scope.myProjects)
    });

    $scope.$on('$destroy', function(){
      socket.unsyncUpdates('project');
    });

    $scope.loadSprint = function()
    {
      console.log($scope.currentProject);

      ProjectService.get({id:$scope.currentProject}, function(project){
        $scope.project = project;
        //socket.syncUpdates('project', $scope.project);
      });
    }

    $scope.onDrop = function($event, task, story, check) {

      var taskcheck = task._id;
      var taskid = _.find(story.tasks,{'_id': task._id})._id;
      console.log(taskcheck);
      console.log(taskid);

      if(taskid === taskcheck)
      {
        var pos = $scope.project.stories.map(function(e) { return e._id; }).indexOf(story._id);
        _.remove($scope.project.stories[pos].tasks, function(task1){
          return task1._id === task._id;
        });
      //console.log(task.notstarted);

        if(check === 'notStarted')
        {
          task['notstarted'] = true;
          task['inprogress'] = false;
          task['done'] = false;
        }
        else if(check === 'inProgress')
        {
          task['notstarted'] = false;
          task['inprogress'] = true;
          task['done'] = false;
        }
        else if(check === 'done')
        {
          task['notstarted'] = false;
          task['inprogress'] = false;
          task['done'] = true;
        }


      $scope.project.stories[pos].tasks.push(task);

      ProjectService.update({
        id: $scope.project._id
      }, $scope.project, function(project) {
        $scope.project = project;
      });
      //socket.syncUpdates('project', $scope.project);

    }
  }


  });
