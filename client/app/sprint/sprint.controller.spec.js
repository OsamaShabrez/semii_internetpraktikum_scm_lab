'use strict';

describe('Controller: SprintCtrl', function () {

  // load the controller's module
  beforeEach(module('a2MeApp'));

  var SprintCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SprintCtrl = $controller('SprintCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
