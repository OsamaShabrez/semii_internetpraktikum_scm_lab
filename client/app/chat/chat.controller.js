'use strict';

angular.module('a2MeApp')
  .controller('ChatCtrl', function($scope, ProjectService, UserService, ChatService, $http, socket, Auth, $filter) {
    this.$http = $http;

    $scope.loadChat = function(currentProjectID) {
      ChatService.query({
        id: currentProjectID
      }, function(chats) {
        $scope.chats = chats[0];
        if (!angular.isDefined(chats[0])) {
          console.log('chat not found, creating one..');
          ChatService.create({'projectid': currentProjectID });
        }
        socket.syncUpdates('chats', $scope.chats);
      });
    }

    $scope.addMessage = function(currentProjectID) {
      if ( angular.isUndefined(currentProjectID) ) {
        console.log('currentProjectID is not defined');
        return;
      }
      ChatService.update({
        id: currentProjectID
      }, {
        "userName": Auth.getCurrentUser().name,
        "message": $scope.justMessage
      });
      $scope.justMessage = '';
      $scope.loadChat(currentProjectID);
    };

    ProjectService.query(function(projects) {
      $scope.myProjects = projects;
      socket.syncUpdates('project', $scope.myProjects);
    });

    $scope.$on('$destroy', function() {
      socket.unsyncUpdates('project');
      socket.unsyncUpdates('chats');
    });


  });
