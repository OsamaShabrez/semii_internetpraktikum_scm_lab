'use strict';

angular.module('a2MeApp')
  .controller('EmailsCtrl', function ($scope, EmailService, socket) {

    EmailService.query(function(emails){
      $scope.emails = emails;
      socket.syncUpdates('emails', $scope.emails);
    });

  });
