'use strict';
angular.module('a2MeApp')
    .config(function ($stateProvider) {
    $stateProvider
        .state('emails', {
        url: '/emails',
        templateUrl: 'app/emails/emails.html',
        controller: 'EmailsCtrl',
        controllerAs: 'mail'
    })
        .state('read-mail', {
        url: '/read-mail',
        templateUrl: 'app/emails/read-mail.html',
        controller: 'EmailsCtrl',
        controllerAs: 'readmail'
    });
});
