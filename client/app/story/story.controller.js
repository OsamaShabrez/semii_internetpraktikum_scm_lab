'use strict';

angular.module('a2MeApp')
  .controller('StoryCtrl', function ($scope, $stateParams, $mdDialog, $mdToast, $state, socket, ProjectService, UserService ) {

    $scope.goBack = function(){
      window.history.back();
    }

    ProjectService.get({id:$stateParams.id}, function(project){
      $scope.project = project;
      socket.syncUpdates('project', $scope.project);
    });

    UserService.query(function(users){
      $scope.users = users;
      socket.syncUpdates('user', $scope.users)
    });

    $scope.$on('$destroy', function() {
      socket.unsyncUpdates('user');
      socket.syncUpdates('project', $scope.project);
    });
/*    $scope.goToStory = function(story){
      console.log(story._id);
      $state.go('task', {
        id : story._id
      });
    }*/

    $scope.deleteStory = function(story, event){
      var confirm = $mdDialog.confirm()
      .title('Delete Story')
      .textContent('Are you sure you want to delete the Story')
      .ariaLabel('Delete')
      .targetEvent(event)
      .openFrom('#left')
      .ok('Yes, Please')
      .cancel('Not Today');

      $mdDialog.show(confirm).then(function(){
        _.remove($scope.project.stories, function(story){
          return story._id === $scope.editstory._id;
        });

      ProjectService.update({
        id: $scope.project._id
      }, $scope.project, function(project) {
        $scope.project = project;
        var toast = $mdToast.simple()
        .textContent('Story Deleted')
        .action('OK')
        .highlightAction(false)
        .position('top right');
        $mdToast.show(toast);
        $scope.editstory = undefined;
      });

    });
      socket.syncUpdates('project', $scope.project);
    }


    $scope.deleteTask = function(story, task, event){
      var confirm = $mdDialog.confirm()
      .title('Delete Task')
      .textContent('Are you sure you want to delete the Task')
      .ariaLabel('Delete')
      .targetEvent(event)
      //.openFrom('#left')
      .ok('Yes, Please')
      .cancel('Not Today');

      $mdDialog.show(confirm).then(function(){
        _.remove($scope.project.stories[$scope.project.stories.map(function(e) { return e._id; }).indexOf(story._id)].tasks, function(task){
          return task._id === $scope.edittask._id;
        });

      ProjectService.update({
        id: $scope.project._id
      }, $scope.project, function(project) {
        $scope.project = project;
        var toast = $mdToast.simple()
        .textContent('Task Deleted')
        .action('OK')
        .highlightAction(false)
        .position('top right');
        $mdToast.show(toast);
        $scope.edittask = undefined;
      });

    });
      socket.syncUpdates('project', $scope.project);
    }


    $scope.updateStory = function(story, event){

      _.remove($scope.project.stories, function(story){
        return story._id === $scope.editstory._id;
      });

      //console.log($scope.project.stories);

      $scope.project.stories.push(story);

      console.log($scope.project.stories);

      ProjectService.update({
        id: $scope.project._id
      }, $scope.project, function(project) {
        $scope.project = project;
        var toast = $mdToast.simple()
        .textContent('Story Updated')
        .action('OK')
        .highlightAction(false)
        .position('top right');
        $mdToast.show(toast);
        $scope.editstory = undefined;
      });
      socket.syncUpdates('project', $scope.project);
    }


    $scope.updateTask = function(story, task, event){

      var pos = $scope.project.stories.map(function(e) { return e._id; }).indexOf(story._id);

      _.remove($scope.project.stories[pos].tasks, function(task){
        return task._id === $scope.edittask._id;
      });

      //console.log($scope.project.stories);

      $scope.project.stories[pos].tasks.push(task);

      //console.log($scope.project.stories);

      ProjectService.update({
        id: $scope.project._id
      }, $scope.project, function(project) {
        $scope.project = project;
        var toast = $mdToast.simple()
        .textContent('Task Updated')
        .action('OK')
        .highlightAction(false)
        .position('top right');
        $mdToast.show(toast);
        $scope.edittask = undefined;
      });
      socket.syncUpdates('project', $scope.project);
    }

    $scope.editStory = function(story){
      $scope.editstory = story;
    }

    $scope.undoStoryEdit = function() {
      $scope.editstory = undefined;
    }

    $scope.editTask = function(task){
      $scope.edittask = task;
    }

    $scope.undoTaskEdit = function() {
      $scope.edittask = undefined;
    }

    $scope.createStory = function(ev) {
      $mdDialog.show({
        controller: DialogController,
        //template: '<md-dialog aria-label="Mango (Fruit)"> <md-content class="md-padding"> <form name="userForm"> <div layout layout-sm="column"> <md-input-container flex> <label>First Name</label> <input ng-model="user.firstName" placeholder="Placeholder text"> </md-input-container> <md-input-container flex> <label>Last Name</label> <input ng-model="theMax"> </md-input-container> </div> <md-input-container flex> <label>Address</label> <input ng-model="user.address"> </md-input-container> <div layout layout-sm="column"> <md-input-container flex> <label>City</label> <input ng-model="user.city"> </md-input-container> <md-input-container flex> <label>State</label> <input ng-model="user.state"> </md-input-container> <md-input-container flex> <label>Postal Code</label> <input ng-model="user.postalCode"> </md-input-container> </div> <md-input-container flex> <label>Biography</label> <textarea ng-model="user.biography" columns="1" md-maxlength="150"></textarea> </md-input-container> </form> </md-content> <div class="md-actions" layout="row"> <span flex></span> <md-button ng-click="answer(\'not useful\')"> Cancel </md-button> <md-button ng-click="answer(\'useful\')" class="md-primary"> Save </md-button> </div></md-dialog>',
        template: '<md-dialog aria-label="Add New Story" style="min-width:600px">'+
          '<md-content flex class="md-padding">'+
          '<div class="box box-success">'+
              '<div class="box-header with-border">'+
              '<h4><b>Add new Story</b></h4>'+
              '</div>'+
              '<div class="box-body">'+
              '<form name="newStoryForm">'+
              '<div layout-gt-sm="row">'+
                '<md-input-container class="md-block" flex-gt-sm>'+
                  '<label>Name</label>'+
                  '<input md-maxlength="30" required name="name" ng-model="newStory.name">'+
                  '<div ng-messages="newStoryForm.name.$error">'+
                    '<div ng-message="required">This is required.</div>'+
                    '<div ng-message="md-maxlength">The name has to be less than 30 characters long.</div>'+
                  '</div>'+
                '</md-input-container>'+
                '<md-input-container class="md-block" flex-gt-sm>'+
                  '<label>Priority</label>'+
                  '<input required name="priority" type="number" ng-model="newStory.priority" min="1" max="5" />'+
                  '<div ng-messages="newStoryForm.priority.$error">'+
                    '<div ng-message="required">This is required.</div>'+
                  '</div>'+
                '</md-input-container>'+
                '</div>'+
                '<md-input-container flex class="md-block">'+
                  '<label>Description</label>'+
                  '<textarea rows="6" md-maxlength="500" required name="description" ng-model="newStory.description">'+
                  '</textarea>'+
                  '<div ng-messages="newStoryForm.description.$error">'+
                    '<div ng-message="required">This is required.</div>'+
                    '<div ng-message="md-maxlength">The description has to be less than 500 characters long.</div>'+
                  '</div>'+
                '</md-input-container>'+
              '</form>        '+
                '</div>'+
              '</div>'+
              '<div class="box-footer">'+
                '<md-button aria-label="Undo Changes" ng-click="answer(\'cancel\')" class="md-raised pull-right">'+
                  '<i class="fa fa-undo"></i> Cancel'+
                '</md-button>'+
                '<md-button class="md-raised md-primary pull-right" ng-click="answer(newStory)" ariaLabel="Create">'+
                   '<i class="fa fa-save"></i> Create'+
                '</md-button>'+
              '</div>'+
            '</md-content>'+
        '</md-dialog>',
        //templateUrl: 'newstory.html',
        targetEvent: ev,
      })
      .then(function(answer) {
        if(answer != 'cancel' && answer != undefined)
        {
          console.log(answer);
          $scope.project.stories.push(answer);
          ProjectService.update({
            id: $scope.project._id
          }, $scope.project, function(project) {
              $scope.project = project;
              var toast = $mdToast.simple()
              .textContent('Story Created')
              .action('OK')
              .highlightAction(false)
              .position('top right');
              $mdToast.show(toast);
          });
          //console.log(answer);
          socket.syncUpdates('project', $scope.project);
        }

      }, function() {
          $scope.newStory = {}
        console.log('You cancelled the dialog.');
        //$scope.alert = 'You cancelled the dialog.';
      });
    };

    $scope.createTask = function(story,ev) {
      $mdDialog.show({
        locals: {
          users : $scope.users
        },
        bindToController: true,
        controllerAs: 'ctrl',
        controller: DialogController,
        //template: '<md-dialog aria-label="Mango (Fruit)"> <md-content class="md-padding"> <form name="userForm"> <div layout layout-sm="column"> <md-input-container flex> <label>First Name</label> <input ng-model="user.firstName" placeholder="Placeholder text"> </md-input-container> <md-input-container flex> <label>Last Name</label> <input ng-model="theMax"> </md-input-container> </div> <md-input-container flex> <label>Address</label> <input ng-model="user.address"> </md-input-container> <div layout layout-sm="column"> <md-input-container flex> <label>City</label> <input ng-model="user.city"> </md-input-container> <md-input-container flex> <label>State</label> <input ng-model="user.state"> </md-input-container> <md-input-container flex> <label>Postal Code</label> <input ng-model="user.postalCode"> </md-input-container> </div> <md-input-container flex> <label>Biography</label> <textarea ng-model="user.biography" columns="1" md-maxlength="150"></textarea> </md-input-container> </form> </md-content> <div class="md-actions" layout="row"> <span flex></span> <md-button ng-click="answer(\'not useful\')"> Cancel </md-button> <md-button ng-click="answer(\'useful\')" class="md-primary"> Save </md-button> </div></md-dialog>',
        template: '<md-dialog aria-label="Add New Task" style="min-width:600px;">'+
          '<md-content flex class="md-padding">'+
          '<div class="box box-success">'+
              '<div class="box-header with-border">'+
              '<h4><b>Add new Task</b></h4>'+
              '</div>'+
              '<div class="box-body">'+
              '<form name="newTaskForm">'+
              '<div layout-gt-sm="row">'+
                '<md-input-container class="md-block" flex-gt-sm>'+
                  '<label>Name</label>'+
                  '<input md-maxlength="30" required name="name" ng-model="newTask.name">'+
                  '<div ng-messages="newTaskForm.name.$error">'+
                    '<div ng-message="required">This is required.</div>'+
                    '<div ng-message="md-maxlength">The name has to be less than 30 characters long.</div>'+
                  '</div>'+
                '</md-input-container>'+
                '<md-input-container class="md-block" flex-gt-sm>'+
                  '<label>Assigne</label>'+
                  '<md-select required name="assigne" ng-model="newTask.assigne">'+
                    '<md-option ng-repeat="user in ctrl.users" value="{{user.name}}">'+
                      '{{user.name}}'+
                    '</md-option>'+
                  '</md-select>'+
                  '<div ng-messages="newTaskForm.assigne.$error">'+
                    '<div ng-message="required">This is required.</div>'+
                  '</div>'+
                '</md-input-container>'+
                '</div>'+
                '<md-input-container flex class="md-block">'+
                  '<label>Details</label>'+
                  '<textarea rows="7" md-maxlength="1000" required name="details" ng-model="newTask.details">'+
                  '</textarea>'+
                  '<div ng-messages="newTaskForm.details.$error">'+
                    '<div ng-message="required">This is required.</div>'+
                    '<div ng-message="md-maxlength">The details has to be less than 1000 characters long.</div>'+
                  '</div>'+
                '</md-input-container>'+
              '</form>        '+
                '</div>'+
              '</div>'+
              '<div class="box-footer">'+
                '<md-button aria-label="Undo Changes" ng-click="answer(\'cancel\')" class="md-raised pull-right">'+
                  '<i class="fa fa-undo"></i> Cancel'+
                '</md-button>'+
                '<md-button class="md-raised md-primary pull-right" ng-click="answer(newTask)" ariaLabel="Create">'+
                   '<i class="fa fa-save"></i> Create'+
                '</md-button>'+
              '</div>'+
            '</md-content>'+
        '</md-dialog>',
        //templateUrl: 'newstory.html',
        targetEvent: ev,
      })
      .then(function(answer) {
        if(answer != 'cancel' && answer != undefined)
        {
          console.log(answer);
          var pos = $scope.project.stories.map(function(e) { return e._id; }).indexOf(story._id);
          $scope.project.stories[pos].tasks.push(answer);

          ProjectService.update({
            id: $scope.project._id
          }, $scope.project, function(project) {
              $scope.project = project;
              var toast = $mdToast.simple()
              .textContent('Task Created')
              .action('OK')
              .highlightAction(false)
              .position('top right');
              $mdToast.show(toast);
          });
          //console.log(answer);
          socket.syncUpdates('project', $scope.project);
        }

      }, function() {
          $scope.newTask = {}
        console.log('You cancelled the dialog.');
        //$scope.alert = 'You cancelled the dialog.';
      });
    };

  });

  function DialogController($scope, $mdDialog) {
    $scope.hide = function() {
      $mdDialog.hide();
    };
    $scope.cancel = function() {
      $mdDialog.cancel();
    };
    $scope.answer = function(answer) {
      $mdDialog.hide(answer);
    };
  };
