'use strict';

angular.module('a2MeApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('story', {
        url: '/story/:id',
        templateUrl: 'app/story/story.html',
        controller: 'StoryCtrl'
      });
  });
