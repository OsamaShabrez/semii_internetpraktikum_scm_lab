'use strict';

angular.module('a2MeApp', [
  'a2MeApp.auth',
  'a2MeApp.admin',
  'a2MeApp.constants',
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'btford.socket-io',
  'ui.router',
  'ui.bootstrap',
  'validation.match',
  'ngMaterial',
  'ngMessages',
  'autofields',
  'jkuri.slimscroll',
  'vAccordion',
  'ang-drag-drop'
])
  .config(function($urlRouterProvider, $locationProvider) {
    $urlRouterProvider
      .otherwise('/');

    $locationProvider.html5Mode(true);
  });
