'use strict';

angular.module('a2MeApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('project', {
        url: '/project',
        templateUrl: 'app/project/project.html',
        controller: 'ProjectCtrl',
        controllerAs: 'proj'
      })
      .state('newproject', {
        url: '/newproject',
        templateUrl: 'app/project/newproject.html',
        controller: 'ProjectCtrl',
        controllerAs: 'newproj'
      });

  });
