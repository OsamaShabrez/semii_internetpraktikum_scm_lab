'use strict';

angular.module('a2MeApp')
.controller('ProjectCtrl', function ($scope, $state, $mdDialog, $mdToast, ProjectService, UserService, $http, socket, Auth) {
    //$scope.message = 'Hello';
    this.$http = $http;

    var test = [{user:'bilal'},{user:'bilalShabbir'},{user:'arsalan'}];

    $scope.goBack = function(){
      window.history.back();
    }

    UserService.query(function(users){
      $scope.users = users;
      socket.syncUpdates('user', $scope.users)
    });

    ProjectService.query(function(projects){
      $scope.myProjects = projects;
      socket.syncUpdates('project', $scope.myProjects);
    });

    $scope.$on('$destroy', function(){
      socket.unsyncUpdates('project');
      socket.unsyncUpdates('user');
    });

    $scope.goToProject = function(project){
      console.log(project._id);
      $state.go('story', {
        id : project._id
      });
    }

    /*$scope.createProject = function(){
      console.log($scope.newProject);
      ProjectService.save($scope.newProject, function(project){
        $scope.newProject = {};
        $state.go('project');
      })
    }*/

    $scope.updateProject = function(editproject, event){
      console.log(editproject);

      ProjectService.update({
        id: editproject._id
      }, editproject, function(project) {
        $scope.project = project;
        var toast = $mdToast.simple()
        .textContent('Project Updated')
        .action('OK')
        .highlightAction(false)
        .position('top right');
        $mdToast.show(toast);
        $scope.editproject = undefined;
      });

      socket.syncUpdates('project', $scope.myProjects);
    }


    $scope.deleteProject = function(editproject, event){
      var confirm = $mdDialog.confirm()
      .title('Delete Project')
      .textContent('Are you sure you want to delete the Project')
      .ariaLabel('Delete')
      .targetEvent(event)
      .openFrom('#left')
      .ok('Yes, Please')
      .cancel('Not Today');

      $mdDialog.show(confirm).then(function(){
        ProjectService.delete({id: editproject._id}, function(project){
          console.log("Project Deleted");
          var toast = $mdToast.simple()
          .textContent('Project Deleted')
          .action('OK')
          .highlightAction(false)
          .position('top right');
          $mdToast.show(toast);
          $scope.editproject = undefined;
        });
        socket.syncUpdates('project', $scope.myProjects);
    });

      socket.syncUpdates('project', $scope.myProjects);
    }

    $scope.editProject = function(project){
      $scope.editproject = project;
    }

    $scope.undoProjectEdit = function() {
      $scope.editproject = undefined;
    }

    $scope.createProject = function(ev) {
      //console.log(test);
      $mdDialog.show({
        locals: {
          users : $scope.users
        },
        bindToController: true,
        controllerAs: 'ctrl',
        controller: DialogController,
        template: '<md-dialog aria-label="Add New Project" style="min-width:600px">'+
          '<md-content flex class="md-padding">'+
          '<div class="box box-success">'+
              '<div class="box-header with-border">'+
              '<h4><b>Add new Project</b></h4>'+
              '</div>'+
              '<div class="box-body">'+
              '<form name="newProjectForm">'+
              '<div layout-gt-sm="row">'+
                '<md-input-container class="md-block" flex-gt-sm>'+
                  '<label>Name</label>'+
                  '<input md-maxlength="30" required name="name" ng-model="newProject.name">'+
                  '<div ng-messages="newProjectForm.name.$error">'+
                    '<div ng-message="required">This is required.</div>'+
                    '<div ng-message="md-maxlength">The name has to be less than 30 characters long.</div>'+
                  '</div>'+
                '</md-input-container>'+
                '<md-input-container class="md-block" flex-gt-sm>'+
                  '<label>Scrum Master</label>'+
                  '<md-select required name="master" ng-model="newProject.master">'+
                    '<md-option ng-repeat="user in ctrl.users" value="{{user.name}}">'+
                      '{{user.name}}'+
                    '</md-option>'+
                  '</md-select>'+
                  '<div ng-messages="newProjectForm.master.$error">'+
                    '<div ng-message="required">This is required.</div>'+
                  '</div>'+
                '</md-input-container>'+
                '</div>'+
                '<md-input-container class="md-block">'+
                  '<label>Developers</label>'+
                  '<md-select name="developers" multiple="true" required name="master" ng-model="newProject.developers">'+
                    '<md-option ng-repeat="user in ctrl.users" value="{{ user.name }}">'+
                      '{{user.name}}'+
                    '</md-option>'+
                  '</md-select>'+
                  '<div ng-messages="newProjectForm.developers.$error">'+
                    '<div ng-message="required">This is required.</div>'+
                  '</div>'+
                '</md-input-container>'+
                '<md-input-container flex class="md-block">'+
                  '<label>Description</label>'+
                  '<textarea rows="5" md-maxlength="300" required name="description" ng-model="newProject.description">'+
                  '</textarea>'+
                  '<div ng-messages="newProjectForm.description.$error">'+
                    '<div ng-message="required">This is required.</div>'+
                    '<div ng-message="md-maxlength">The description has to be less than 300 characters long.</div>'+
                  '</div>'+
                '</md-input-container>'+
              '</form>        '+
                '</div>'+
              '</div>'+
              '<div class="box-footer">'+
                '<md-button aria-label="Undo Changes" ng-click="answer(\'cancel\')" class="md-raised pull-right">'+
                  '<i class="fa fa-undo"></i> Cancel'+
                '</md-button>'+
                '<md-button class="md-raised md-primary pull-right" ng-click="answer(newProject)" ariaLabel="Create">'+
                   '<i class="fa fa-save"></i> Create'+
                '</md-button>'+
              '</div>'+
            '</md-content>'+
        '</md-dialog>',
        //templateUrl: 'newstory.html',
        targetEvent: ev,
      })
      .then(function(answer) {
        if(answer != 'cancel' && answer != undefined)
        {
          console.log(answer);
          answer['owner'] = Auth.getCurrentUser().name;
          console.log(Auth.getCurrentUser().name);
          ProjectService.save(answer, function(project){
            console.log(project);
            $scope.newProject = {};
          })
          socket.syncUpdates('project', $scope.myProjects)
          //console.log(answer);
        }

      }, function() {
          $scope.newProject = {}
        console.log('You cancelled the dialog.');
        //$scope.alert = 'You cancelled the dialog.';
      });
    };

  });

  function DialogController($scope, $mdDialog, UserService) {
    $scope.hide = function() {
      $mdDialog.hide();
    };
    $scope.cancel = function() {
      $mdDialog.cancel();
    };
    $scope.answer = function(answer) {
      $mdDialog.hide(answer);
    };

  };
