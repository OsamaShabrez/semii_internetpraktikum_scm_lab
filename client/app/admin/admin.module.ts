'use strict';

angular.module('a2MeApp.admin', [
  'a2MeApp.auth',
  'ui.router'
]);
