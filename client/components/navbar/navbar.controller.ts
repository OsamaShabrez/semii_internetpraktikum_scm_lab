'use strict';

class NavbarController {
  //start-non-standard
  menu = [{
    'title': 'Dashboard',
    'state': 'active',
    'class': 'fa fa-dashboard',
    'ref'  : 'main',
    'color': '',
    'no'   : ''
    },{
    'title': 'Projects',
    'state': '',
    'class': 'fa fa-folder',
    'ref'  : 'project',
    'color': '',//'bg-aqua',
    'no'   : '',//'2'
    },{
    'title': 'Sprint Board',
    'state': '',
    'class': 'fa fa-dashboard',
    'ref'  : 'sprint',
    'color': '',
    'no'   : ''
  },{
    'title': 'Calendar',
    'state': '',
    'class': 'fa fa-calendar',
    'ref'  : 'calendar',
    'color': 'bg-red',
    'no'   : ''//'3'
    },{
    'title': 'Mailbox',
    'state': '',
    'class': 'fa fa-envelope',
    'ref'  : 'emails',
    'color': '',//'bg-yellow',
    'no'   : ''//'12'
  },{
    'title': 'Chat',
    'state': '',
    'class': 'fa fa-wechat',
    'ref'  : 'chat',
    'color': '',//'bg-yellow',
    'no'   : ''//'5'
  }
      ];

  isCollapsed = true;
  //end-non-standard

  constructor(Auth) {
    this.isLoggedIn = Auth.isLoggedIn;
    this.isAdmin = Auth.isAdmin;
    this.getCurrentUser = Auth.getCurrentUser;
  }
}

angular.module('a2MeApp')
  .controller('NavbarController', NavbarController);
