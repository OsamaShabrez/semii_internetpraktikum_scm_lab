'use strict';

angular.module('a2MeApp.auth', [
  'a2MeApp.constants',
  'a2MeApp.util',
  'ngCookies',
  'ui.router'
])
  .config(function($httpProvider) {
    $httpProvider.interceptors.push('authInterceptor');
  });
